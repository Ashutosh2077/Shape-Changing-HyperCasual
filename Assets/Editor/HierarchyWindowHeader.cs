﻿using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
public static class HierarchySectionHeader
{
    static HierarchySectionHeader()
    {
        EditorApplication.hierarchyWindowItemOnGUI += HierarchyWindowItemOnGUI;
    }

    static void HierarchyWindowItemOnGUI(int instanceID, Rect selectionRect)
    {
        var gameObject = EditorUtility.InstanceIDToObject(instanceID) as GameObject;

        if (gameObject != null && gameObject.name.StartsWith("BLACK_", System.StringComparison.Ordinal))
        {
            EditorGUI.DrawRect(selectionRect, Color.black);
            EditorGUI.DropShadowLabel(selectionRect, gameObject.name.Replace("BLACK_", "").ToUpperInvariant());
        }
        if (gameObject != null && gameObject.name.StartsWith("RED_", System.StringComparison.Ordinal))
        {
            EditorGUI.DrawRect(selectionRect, Color.red);
            EditorGUI.DropShadowLabel(selectionRect, gameObject.name.Replace("RED_", "").ToUpperInvariant());
        }
        if (gameObject != null && gameObject.name.StartsWith("GREEN_", System.StringComparison.Ordinal))
        {
            EditorGUI.DrawRect(selectionRect, Color.green);
            EditorGUI.DropShadowLabel(selectionRect, gameObject.name.Replace("GREEN_", "").ToUpperInvariant());
        }
        if (gameObject != null && gameObject.name.StartsWith("YELLOW_", System.StringComparison.Ordinal))
        {
            EditorGUI.DrawRect(selectionRect, Color.yellow);
            EditorGUI.DropShadowLabel(selectionRect, gameObject.name.Replace("YELLOW_", "").ToUpperInvariant());
        }
    }
}