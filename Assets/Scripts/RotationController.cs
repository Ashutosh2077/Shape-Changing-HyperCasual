using UnityEngine;

public class RotationController : MonoBehaviour
{
    public float rotationSpeed = 50f;
    public enum RotationAxis
    {
        X,
        Y,
        Z
    }
    public RotationAxis rotationAxis = RotationAxis.X;


    void Update()
    {
        // Calculate the rotation vector based on the chosen axis
        Vector3 rotation = GetRotationVector();

        // Apply rotation to the GameObject
        transform.Rotate(rotation * rotationSpeed * Time.deltaTime);
    }

    Vector3 GetRotationVector()
    {
        switch (rotationAxis)
        {
            case RotationAxis.X:
                return new Vector3(1f, 0f, 0f);
            case RotationAxis.Y:
                return new Vector3(0f, 1f, 0f);
            case RotationAxis.Z:
                return new Vector3(0f, 0f, 1f);
            default:
                return Vector3.zero;
        }
    }
}
