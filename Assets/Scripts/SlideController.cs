using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlideController : MonoBehaviour
{
    public float Speed = 3;
    public Rigidbody rb;
    public bool AIControled = false;
    public int VehicleIndex;
    public PlayerController.ObjectSet activeObjectSet;
    public AIPlayersControler.ObjectSet AIactiveObjectSet;
    bool isactive = false;
    private void OnDisable()
    {
        isactive = false;
    }
    private void OnEnable()
    {
        isactive = true;
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionZ;
    }
    private void Start()
    {
        activeObjectSet = PlayerController.Instance.vehiclesSet[VehicleIndex];
    }
    void Update()
    {
        Drive();
    }

    public void Drive()
    {
        Debug.Log("Cart is driving!");
        transform.Translate(-Speed * Time.deltaTime, 0, 0);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (isactive)
        {
            if (AIControled)
            {
                if (collision.gameObject.CompareTag("BrokenRoad") && (AIactiveObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("Dock") && (AIactiveObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("NormalRoad") && (AIactiveObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("ClimbableWall") && (AIactiveObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("TrainTrack") && (AIactiveObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("WaterSlide") && (AIactiveObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 3;
                }
                else if (collision.gameObject.CompareTag("Rope") && (AIactiveObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 1;
                }
            }
            else
            {
                if (collision.gameObject.CompareTag("BrokenRoad") && (activeObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("NormalRoad") && (activeObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("Dock") && (activeObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("ClimbableWall") && (activeObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 0f;
                }
                else if (collision.gameObject.CompareTag("TrainTrack") && (activeObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("WaterSlide") && (activeObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 3;
                }
                else if (collision.gameObject.CompareTag("Rope") && (activeObjectSet.controller == GetComponent<SlideController>()))
                {
                    Speed = 1;
                }
            }
        }
    }
}
