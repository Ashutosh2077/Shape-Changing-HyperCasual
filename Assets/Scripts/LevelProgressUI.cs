﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class LevelProgressUI : MonoBehaviour
{
    [Header("UI references :")]
    [SerializeField] private Image uiFillImage;
    [SerializeField] private Text uiStartText;
    [SerializeField] private Text uiEndText;
    [SerializeField] private List<Transform> playerTransforms; // List of player transforms
    [SerializeField] private List<Image> playerDots; // List of player dots

    [Header("Endline references :")]
    [SerializeField] private Transform endLineTransform;

    private Vector3 endLinePosition;
    private Dictionary<Transform, float> fullDistances; 

    private void Start()
    {
        endLinePosition = endLineTransform.position;
        InitializeFullDistances();
    }

    private void InitializeFullDistances()
    {
        fullDistances = new Dictionary<Transform, float>();

        foreach (Transform playerTransform in playerTransforms)
        {
            fullDistances[playerTransform] = GetDistance(playerTransform);
        }
    }

    public void SetLevelTexts(int level)
    {
        uiStartText.text = level.ToString();
        uiEndText.text = (level + 1).ToString();
    }

    private float GetDistance(Transform playerTransform)
    {
        return (endLinePosition - playerTransform.position).sqrMagnitude;
    }

    private void UpdateProgressFill(float value, Image playerDot)
    {
        uiFillImage.fillAmount = value;

        float dotPositionX = Mathf.Lerp(uiFillImage.rectTransform.rect.xMin, uiFillImage.rectTransform.rect.xMax, value);
        playerDot.rectTransform.anchoredPosition = new Vector2(dotPositionX, playerDot.rectTransform.anchoredPosition.y);
    }

    private void Update()
    {
        foreach (Transform currentPlayerTransform in playerTransforms)
        {
            if (currentPlayerTransform.position.z <= endLinePosition.z)
            {
                float newDistance = GetDistance(currentPlayerTransform);
                float progressValue = Mathf.InverseLerp(fullDistances[currentPlayerTransform], 0f, newDistance);

                Image currentPlayerDot = playerDots[playerTransforms.IndexOf(currentPlayerTransform)];
                UpdateProgressFill(progressValue, currentPlayerDot);
            }
        }
    }
}
