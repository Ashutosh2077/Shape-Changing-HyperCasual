using UnityEngine;

public class CartController : MonoBehaviour
{
    public float Speed = 3;
    public Rigidbody rb;
    public bool AIControled = false;
    public int VehicleIndex;
    public PlayerController.ObjectSet activeObjectSet;
    public AIPlayersControler.ObjectSet AIactiveObjectSet;
    bool isactive = false;
    private void OnDisable()
    {
        isactive = false;
    }
    private void OnEnable()
    {
        isactive = true;
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
    }
    private void Start()
    {
        activeObjectSet = PlayerController.Instance.vehiclesSet[VehicleIndex];
    }
    void Update()
    {
        Drive();
    }

    public void Drive()
    {
        Debug.Log("Cart is driving!");
        transform.Translate(-Speed * Time.deltaTime, 0, 0);
    }
    
    private void OnCollisionStay(Collision collision)
    {
        if (isactive)
        {
            if (AIControled)
            {
                if (collision.gameObject.CompareTag("BrokenRoad") && (AIactiveObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("Dock") && (AIactiveObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("NormalRoad") && (AIactiveObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 2;
                }
                else if (collision.gameObject.CompareTag("ClimbableWall") && (AIactiveObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("TrainTrack") && (AIactiveObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 4;
                }
                else if (collision.gameObject.CompareTag("WaterSlide") && (AIactiveObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("Rope") && (AIactiveObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 0;
                }
            }
            else
            {
                if (collision.gameObject.CompareTag("BrokenRoad") && (activeObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("NormalRoad") && (activeObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 2;
                }
                else if (collision.gameObject.CompareTag("Dock") && (activeObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("ClimbableWall") && (activeObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 0f;
                }
                else if (collision.gameObject.CompareTag("TrainTrack") && (activeObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 4;
                }
                else if (collision.gameObject.CompareTag("WaterSlide") && (activeObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("Rope") && (activeObjectSet.controller == GetComponent<CartController>()))
                {
                    Speed = 0;
                }
            }
        }
    }
}
