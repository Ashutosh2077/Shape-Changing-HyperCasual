using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using System.Collections.Generic;

public class PlayerController : MonoBehaviour
{
    public CinemachineVirtualCamera cinemachineCamera;
    public List<ObjectSet> vehiclesSet = new List<ObjectSet>();
    public Sprite normalSprite;
    public Sprite clickedSprite;

    public GameObject Confetti;
    public GameObject WonPannel;

    public static PlayerController Instance;
    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
        foreach (var objectSet in vehiclesSet)
        {
            objectSet.button.onClick.AddListener(() => ActivateObject(objectSet));
        }
    }

    void ActivateObject(ObjectSet objectSet)
    {
        foreach (var objSet in vehiclesSet)
        {
            objSet.button.image.sprite = normalSprite;
            objSet.obj.SetActive(false);
            objSet.collider.enabled = false;

            if (objSet.controller != null)
            {
                objSet.controller.enabled = false;
            }
        }

        // Activate the selected object
        objectSet.obj.SetActive(true);

        if (objectSet.controller != null)
        {
            objectSet.controller.enabled = true;
            objectSet.collider.enabled = true;
        }
        else
        {
            Debug.LogError("Controller script not found on the activated object.");
        }

        // Change the source image of the clicked button
        objectSet.button.image.sprite = clickedSprite;

        if (objectSet.cameraFollow != null)
        {
            cinemachineCamera.Follow = objectSet.cameraFollow;
            cinemachineCamera.LookAt = objectSet.cameraFollow;
        }
        else
        {
            Debug.LogError("CameraFollow transform not found in the activated object's children.");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("End"))
        {
            //foreach (var objSet in vehiclesSet)
            //{
            //    if (objSet.controller != null)
            //    {
            //        objSet.controller.enabled = false;
            //    }
            //}
        }
        Confetti.SetActive(true);
        Invoke("waitDelay",2);
    }
    public void waitDelay()
    {
        WonPannel.SetActive(true);
    }
    [System.Serializable]
    public struct ObjectSet
    {
        public GameObject obj;
        public Button button;
        public Transform cameraFollow;
        public Collider collider;
        public MonoBehaviour controller;
    }
}
