using UnityEngine;

public class RollerController : MonoBehaviour
{
    public float Speed = 3;
    public Mesh newRoadMesh;
    public Rigidbody rb;
    public bool AIControled = false;
    public int VehicleIndex;
    public PlayerController.ObjectSet activeObjectSet;
    public AIPlayersControler.ObjectSet AIactiveObjectSet;
    bool isactive = false;
    public Animator animator;
    private void OnDisable()
    {
        isactive = false;
    }
    private void OnEnable()
    {
        isactive = true;
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
    }
    private void Start()
    {
        activeObjectSet = PlayerController.Instance.vehiclesSet[VehicleIndex];
    }
    void Update()
    {
        Drive();
    }

    public void Drive()
    {
        Debug.Log("Roller is driving!");
        transform.Translate(-Speed * Time.deltaTime, 0, 0);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (isactive)
        {
            if (AIControled)
            {
                if (collision.gameObject.CompareTag("BrokenRoad") && (AIactiveObjectSet.controller == GetComponent<RollerController>()))
                {
                    TransformRoad(collision.gameObject);
                    Speed = 2;
                    animator.SetBool("Move", true);
                }
                else if (collision.gameObject.CompareTag("Dock") && (AIactiveObjectSet.controller == GetComponent<RollerController>()))
                {
                    Speed = 0;
                    animator.SetBool("Move", false);

                }
                else if (collision.gameObject.CompareTag("NormalRoad") && (AIactiveObjectSet.controller == GetComponent<RollerController>()))
                {
                    Speed = 3;
                    animator.SetBool("Move", true);

                }
                else if (collision.gameObject.CompareTag("ClimbableWall") && (AIactiveObjectSet.controller == GetComponent<RollerController>()))
                {
                    Speed = 0;
                    animator.SetBool("Move", false);

                }
            }
            else
            {
                if (collision.gameObject.CompareTag("BrokenRoad") && (activeObjectSet.controller == GetComponent<RollerController>()))
                {
                    TransformRoad(collision.gameObject);
                    animator.SetBool("Move", true);

                    Speed = 2f;
                }
                else if (collision.gameObject.CompareTag("NormalRoad") && (activeObjectSet.controller == GetComponent<RollerController>()))
                {
                    Speed = 2.5f;
                    animator.SetBool("Move", true);

                }
                else if (collision.gameObject.CompareTag("Dock") && (activeObjectSet.controller == GetComponent<RollerController>()))
                {
                    Speed = 0.2f;
                    animator.SetBool("Move", false);
                }
                else if (collision.gameObject.CompareTag("ClimbableWall") && (activeObjectSet.controller == GetComponent<RollerController>()))
                {
                    Speed = 0f;
                    animator.SetBool("Move", false);
                }
            }
        }
    }
    public void TransformRoad(GameObject roadObject)
    {
        MeshFilter meshFilter = roadObject.GetComponent<MeshFilter>();
        MeshCollider meshCollider = roadObject.GetComponent<MeshCollider>();
        if (meshFilter != null)
        {
            meshFilter.mesh = newRoadMesh;
            meshCollider.sharedMesh = newRoadMesh;
            roadObject.transform.Rotate(0f, 90f, 0f);
            roadObject.tag = "NormalRoad";
        }
        else
        {
            Debug.LogError("MeshFilter component not found on the road roller GameObject.");
        }
    }
}
