using System.Collections;
using UnityEngine;
using System.Collections.Generic;

public class AIPlayersControler : MonoBehaviour
{
    public static AIPlayersControler Instance;
    public List<ObjectSet> vehiclesSet = new List<ObjectSet>();

    public Vector2 timeDelay = new Vector2(2f, 8f);
    void Awake()
    {
        Instance = this;
    }

    void Start()
    {
    }

    public IEnumerator ActivateObject(ObjectSet objectSet, float delay)
    {
        yield return new WaitForSeconds(delay);
        foreach (var objSet in vehiclesSet)
        {
            objSet.obj.SetActive(false);
            objSet.collider.enabled = false;

            if (objSet.controller != null)
            {
                objSet.controller.enabled = false;
            }
        }

        // Activate the selected object
        objectSet.obj.SetActive(true);

        if (objectSet.controller != null)
        {
            objectSet.controller.enabled = true;
            objectSet.collider.enabled = true;
        }
        else
        {
            Debug.LogError("Controller script not found on the activated object.");
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("End"))
        {
            foreach (var objSet in vehiclesSet)
            {
                if (objSet.controller != null)
                {
                    objSet.controller.enabled = false;
                }
            }
        }
    }

    [System.Serializable]
    public struct ObjectSet
    {
        public GameObject obj;
        public Collider collider;
        public MonoBehaviour controller;
    }
}
