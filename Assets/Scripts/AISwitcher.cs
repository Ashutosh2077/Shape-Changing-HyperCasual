using UnityEngine;

public class AISwitcher : MonoBehaviour
{
    [Header("Delay Slider")]
    public Vector2 timeDelay = new Vector2(2f, 8f);
    public int switchIndex=0;
    public AIPlayersControler aIPlayer;
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("AIPlayer"))
        {
            Debug.Log("Switching vehicle of " + other.gameObject.name);
            if (aIPlayer != null)
            {
                aIPlayer.StartCoroutine(aIPlayer.ActivateObject(aIPlayer.vehiclesSet[switchIndex], Random.Range(timeDelay.x, timeDelay.y)));
            }
            else
            {
                Debug.LogError("AIPlayersControler not found in the scene.");
            }
        }
    }
}
