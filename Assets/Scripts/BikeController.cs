using UnityEngine;

public class BikeController : MonoBehaviour
{
    public float Speed = 4;
    public Rigidbody rb;
    public bool AIControled = false;
    public int VehicleIndex;
    public PlayerController.ObjectSet activeObjectSet;
    public AIPlayersControler.ObjectSet AIactiveObjectSet;
    bool isactive = false;
    public Animator animator;

    private void OnDisable()
    {
        isactive = false;
    }
    private void OnEnable()
    {
        isactive = true;
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezePositionZ;
    }
    private void Start()
    {
        activeObjectSet = PlayerController.Instance.vehiclesSet[VehicleIndex];
    }
    void Update()
    {
        Drive();
    }

    public void Drive()
    {
        // Add specific bike driving logic here
        Debug.Log("Bike is driving!");
        transform.Translate(-Speed * Time.deltaTime, 0, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isactive)
        {
            if (AIControled)
            {
                if (collision.gameObject.CompareTag("BrokenRoad") && (AIactiveObjectSet.controller == GetComponent<BikeController>()))
                {
                    Speed = 0f;
                    animator.SetBool("Move", true);

                }
                else if (collision.gameObject.CompareTag("NormalRoad") && (AIactiveObjectSet.controller == GetComponent<BikeController>()))
                {
                    Speed = 4f;
                    animator.SetBool("Move", true);

                }
                else if (collision.gameObject.CompareTag("Dock") && (AIactiveObjectSet.controller == GetComponent<BikeController>()))
                {
                    Speed = 3;
                    animator.SetBool("Move", true);

                }
                else if (collision.gameObject.CompareTag("ClimbableWall") && (AIactiveObjectSet.controller == GetComponent<BikeController>()))
                {
                    Speed = 0;
                    animator.SetBool("Move", false);

                }
            }
            else
            {

                if (collision.gameObject.CompareTag("BrokenRoad") && (activeObjectSet.controller == GetComponent<BikeController>()))
                {
                    Speed = 0f;
                    animator.SetBool("Move", false);

                }
                else if (collision.gameObject.CompareTag("NormalRoad") && (activeObjectSet.controller == GetComponent<BikeController>()))
                {
                    Speed = 4f;
                    animator.SetBool("Move", true);

                }
                else if (collision.gameObject.CompareTag("Dock") && (activeObjectSet.controller == GetComponent<BikeController>()))
                {
                    Speed = 3f;
                    animator.SetBool("Move", true);

                }
                else if (collision.gameObject.CompareTag("ClimbableWall") && (activeObjectSet.controller == GetComponent<BikeController>()))
                {
                    Speed = 0f;
                    animator.SetBool("Move", false);

                }
            }
        }
    }
}
