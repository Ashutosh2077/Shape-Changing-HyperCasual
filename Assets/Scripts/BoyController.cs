using UnityEngine;

public class BoyController : MonoBehaviour
{
    public float Speed = 2;
    public Rigidbody rb;
    public Animator animator;
    public bool AIControled = false;
    public int VehicleIndex;
    public PlayerController.ObjectSet activeObjectSet;
    public AIPlayersControler.ObjectSet AIactiveObjectSet;
    public float raycastHeight = 0.0f; // Set the desired height for the raycast

    bool isactive = false;
    private void OnDisable()
    {
        isactive = false;
    }
    private void OnEnable()
    {
        this.transform.rotation = Quaternion.Euler(0, 0, 5);
        isactive = true;
        rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionZ;
    }
    private void Start()
    {
        activeObjectSet = PlayerController.Instance.vehiclesSet[VehicleIndex];
    }
    void Update()
    {
        Drive();
    }

    public void Drive()
    {
        CheckForClimbableWall();
        Debug.Log("Boy is driving!");
        transform.Translate(-Speed * Time.deltaTime, 0, 0);
        animator.SetFloat("Speed", Speed);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isactive)
        {

            if (AIControled)
            {
                if (collision.gameObject.CompareTag("BrokenRoad") && (AIactiveObjectSet.controller == GetComponent<BoyController>()))
                {
                    Speed = 1.5f;
                }
                else if (collision.gameObject.CompareTag("NormalRoad") && (AIactiveObjectSet.controller == GetComponent<BoyController>()))
                {
                    Speed = 2;
                }
                else if (collision.gameObject.CompareTag("Dock") && (AIactiveObjectSet.controller == GetComponent<BoyController>()))
                {
                    Speed = 2;
                }
                else if (collision.gameObject.CompareTag("TrainTrack") && (AIactiveObjectSet.controller == GetComponent<BoyController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("Rope") && (AIactiveObjectSet.controller == GetComponent<BoyController>()))
                {
                    Speed = 0;

                }
            }
            else
            {
                if (collision.gameObject.CompareTag("BrokenRoad") && (activeObjectSet.controller == GetComponent<BoyController>()))
                {
                    Speed = 1.5f;
                }
                else if (collision.gameObject.CompareTag("NormalRoad") && (activeObjectSet.controller == GetComponent<BoyController>()))
                {
                    Speed = 2f;
                }
                else if (collision.gameObject.CompareTag("Dock") && (activeObjectSet.controller == GetComponent<BoyController>()))
                {
                    Speed = 2f;
                }
                else if (collision.gameObject.CompareTag("TrainTrack") && (activeObjectSet.controller == GetComponent<BoyController>()))
                {
                    Speed = 0;
                }
                else if (collision.gameObject.CompareTag("Rope") && (activeObjectSet.controller == GetComponent<BoyController>()))
                {
                    Speed = 0;
                }
            }
        }
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position + Vector3.up * raycastHeight, -transform.right * 0.3f); // Change the ray length as needed
    }

    private void CheckForClimbableWall()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position + Vector3.up * raycastHeight, -transform.right, out hit, 0.3f)) // Change the ray length as needed
        {
            if (hit.collider.CompareTag("ClimbableWall"))
            {
                rb.AddForce(Vector3.up * 10 * Time.deltaTime);
                rb.useGravity = false;
                Speed = 0.5f;
                animator.SetBool("Climb", true);
            }
            
        }
        else
        {
            // If not hitting anything, reset gravity and speed
            rb.useGravity = true;
            animator.SetBool("Climb", false);
        }
    }


}
